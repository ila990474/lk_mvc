<?php

require "config.php";
require "Controller.php";
require "Database.php";
require "Model.php";


class Boot {
	
	protected $controller = 'Index'; //default controller
	protected $action = 'index'; //default action
	protected $param = [];

	public function __construct (){
		//echo "booting sukses";
		
		$url = $_GET['r'];
$url = $this->parseUrl($url); // Menggunakan metode parseUrl


		if (file_exists ('apps/controllers/'.$url[0].'.php')){ //mengecek apakah file ada 
			$this->controller = $url[0];
			unset($url[0]);
		}
        // membuat instance dari controler yg dipilih users/controller default
		require ('apps/controllers/'.$this->controller.'.php');
		$this->controller = new $this->controller; //instance baru controller, isikan di variable controller

		if (isset($url[1])){
            if (method_exists($this->controller, $url[1])){
                $this->action = $url[1];
				unset($url[1]); //setelah digunakan, 1 dihilangkan dari array url
			}
		}

	
        if (!empty($url)){
            $this->param = array_values($url);
        }

		//memanggil method di dalam objek beserta parameternya
		call_user_func_array([$this->controller,$this->action],$this->param);
		//var_dump($url);
	}

    //routing aplikasi MVC -->memilihkan controller, model, dan view yang diperlukan
	public function parseURL($url) {
		if (isset($_GET['r'])) {
			$url = rtrim($_GET['r'], '/');
			$url = filter_var($url, FILTER_SANITIZE_URL);
			$url = explode('/', $url);
		}
		return $url;
	}
}
